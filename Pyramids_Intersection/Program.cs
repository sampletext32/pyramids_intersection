﻿using System;
using System.Collections.Generic;

namespace Pyramids_Intersection
{
    public class Vector3F
    {
        public float X { get; }
        public float Y { get; }
        public float Z { get; }

        public Vector3F Invert()
        {
            return new Vector3F(-X, -Y, -Z);
        }

        public Vector3F Subtract(Vector3F vec)
        {
            return Add(vec.Invert());
        }

        public Vector3F Add(Vector3F vec)
        {
            return new Vector3F(X + vec.X, Y + vec.Y, Z + vec.Z);
        }

        public float Length()
        {
            return (float) Math.Sqrt(X * X + Y * Y + Z * Z);
        }

        public Vector3F Normalize()
        {
            float length = Length();
            return new Vector3F(X / length, Y / length, Z / length);
        }

        public static Vector3F Input(string header = "")
        {
            Console.Write(header + ": ");
            var s = Console.ReadLine()?.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            if (s != null)
            {
                float x = float.Parse(s[0]);
                float y = float.Parse(s[1]);
                float z = float.Parse(s[2]);
                return new Vector3F(x, y, z);
            }

            return new Vector3F(0f, 0f, 0f);
        }

        public override string ToString()
        {
            return X + " " + Y + " " + Z;
        }

        public Vector3F(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }

    public class Pyramid
    {
        public Vector3F P1 { get; private set; }
        public Vector3F P2 { get; private set; }
        public Vector3F P3 { get; private set; }
        public Vector3F P4 { get; private set; }

        public bool CheckIntersection(Pyramid pyramid)
        {
            foreach (var face in EnumerateFaces())
            {
                var d = 0f;
                var planeNormal = Utils.PointsToPlane(face.Item1, face.Item2, face.Item3, ref d);
                foreach (var edge in pyramid.EnumerateEdges())
                {
                    var intersectionPoint = Utils.FindIntersectionPointOfLineAndPlane(planeNormal, d,
                        Utils.PointsToVector(edge.Item1, edge.Item2), edge.Item1);

                    var isInsideFace =
                        Utils.IsPointInsideTriangle(face.Item1, face.Item2, face.Item3, intersectionPoint);
                    var isInsideEdge = Utils.IsPointInsideEdge(edge.Item1, edge.Item2, intersectionPoint);

                    if (isInsideFace && isInsideEdge)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public IEnumerable<Tuple<Vector3F, Vector3F, Vector3F>> EnumerateFaces()
        {
            yield return new Tuple<Vector3F, Vector3F, Vector3F>(P1, P2, P3);
            yield return new Tuple<Vector3F, Vector3F, Vector3F>(P1, P2, P4);
            yield return new Tuple<Vector3F, Vector3F, Vector3F>(P2, P3, P4);
            yield return new Tuple<Vector3F, Vector3F, Vector3F>(P1, P3, P4);
        }

        public IEnumerable<Tuple<Vector3F, Vector3F>> EnumerateEdges()
        {
            yield return new Tuple<Vector3F, Vector3F>(P1, P2);
            yield return new Tuple<Vector3F, Vector3F>(P1, P3);
            yield return new Tuple<Vector3F, Vector3F>(P1, P4);
            yield return new Tuple<Vector3F, Vector3F>(P2, P3);
            yield return new Tuple<Vector3F, Vector3F>(P2, P3);
            yield return new Tuple<Vector3F, Vector3F>(P3, P4);
        }

        public static Pyramid Input()
        {
            Console.WriteLine("Input Pyramid: ");
            return new Pyramid(Vector3F.Input("Point 1"), Vector3F.Input("Point 2"), Vector3F.Input("Point 3"),
                Vector3F.Input("Point 4"));
        }

        public Pyramid(Vector3F p1, Vector3F p2, Vector3F p3, Vector3F p4)
        {
            P1 = p1;
            P2 = p2;
            P3 = p3;
            P4 = p4;
        }
    }

    public class Utils
    {
        //Направляющий вектор прямой по 2 точкам
        public static Vector3F PointsToVector(Vector3F p1, Vector3F p2)
        {
            return p2.Subtract(p1);
        }

        //Формула Герона
        public static float Square(Vector3F p1, Vector3F p2, Vector3F p3)
        {
            var side1 = p2.Subtract(p1).Length();
            var side2 = p3.Subtract(p2).Length();
            var side3 = p3.Subtract(p1).Length();
            var p = (side1 + side2 + side3) / 2;
            return (float) Math.Sqrt(p * (p - side1) * (p - side2) * (p - side3));
        }

        //Уравнение плоскости по 3 точкам (выведено из матрицы)
        public static Vector3F PointsToPlane(Vector3F p0, Vector3F p1, Vector3F p2, ref float d)
        {
            var a = p1.Y * p2.Z - p1.Y * p0.Z - p0.Y * p2.Z - p1.Z * p2.Y + p1.Z * p0.Y + p0.Z * p2.Y;
            var b = -(p1.X * p2.Z - p1.X * p0.Z - p0.X * p2.Z - p1.Z * p2.X + p1.Z * p0.X + p0.Z * p2.X);
            var c = p1.X * p2.Y - p1.X * p0.Y - p0.X * p2.Y - p1.Y * p2.X + p1.Y * p0.X + p0.Y * p2.X;
            d = -p0.X * a + p0.Y * b - p0.Z * c;
            return new Vector3F(a, b, c);
        }

        //Найти точку пересечения прямой и плоскости (http://cyclowiki.org/wiki/Точка_пересечения_прямой_и_плоскости)
        public static Vector3F FindIntersectionPointOfLineAndPlane(Vector3F planeNormal, float planeD, Vector3F line,
            Vector3F linePoint)
        {
            var move =
                (planeNormal.X * linePoint.X + planeNormal.Y * linePoint.Y + planeNormal.Z * linePoint.Z + planeD) /
                (line.X * planeNormal.X + line.Y * planeNormal.Y + line.Z * planeNormal.Z);
            var x = linePoint.X - line.X * move;
            var y = linePoint.Y - line.Y * move;
            var z = linePoint.Z - line.Z * move;
            return new Vector3F(x, y, z);
        }

        //Принадлежит ли точка ребру
        public static bool IsPointInsideEdge(Vector3F edgePoint1, Vector3F edgePoint2, Vector3F point)
        {
            //если сумма расстояний от точки до краёв отрезка равна длине отрезка, то точка принадлежит
            return Math.Abs(PointsToVector(point, edgePoint1).Length() +
                            PointsToVector(point, edgePoint2).Length() -
                            PointsToVector(edgePoint1, edgePoint2).Length()) < 0.01f;
        }

        //Принадлежит ли точка треугольнику (функция была придумана самостоятельно)
        public static bool IsPointInsideTriangle(Vector3F a, Vector3F b, Vector3F c, Vector3F point)
        {
            //Если сумма площадей треугольников, 
            //построенных на сторонах и 2 векторах из их вершин к точке, 
            //равны или меньше площади треугольника, значит точка лежит в треугольнике
            //меньше площади треугольника, значит точка лежит вне треугольника
            var squareAb = Square(point, a, b);
            var squareBc = Square(point, b, c);
            var squareAc = Square(point, a, c);
            var squareAbc = Square(a, b, c);

            //точка внутри или на грани треугольника
            return (squareAb + squareBc + squareAc - squareAbc) < 0.01f;
        }
    }

    public class Program
    {
        public static void Main()
        {
            var pyramid1 = new Pyramid(new Vector3F(2, -2, -2), new Vector3F(2, 0, -2), new Vector3F(0, -2, -2),
                new Vector3F(0, 0, -0.1f));
            var pyramid2 = new Pyramid(new Vector3F(-1, -1, 0), new Vector3F(1, 1, 0), new Vector3F(1, -1, 0),
                new Vector3F(-1, 1, 2));

            Console.WriteLine(pyramid1.CheckIntersection(pyramid2));
            Console.ReadKey();
        }
    }
}